<?php

Route::get('/sign-up', 'PageController@signUpPage')->name('sign-up');
Route::post('/sign-up', 'AuthController@signUp')->name('sign-up');


Route::get('/sign-in', 'PageController@signInPage')->name('sign-in');
Route::post('/sign-in', 'AuthController@signIn')->name('sign-in');

Route::get('/sign-out', 'AuthController@signOut');

Route::get('sign-in/facebook', 'FacebookLoginController@redirectToProvider');
Route::get('sign-in/facebook/callback', 'FacebookLoginController@handleProviderCallback');

Route::get('sign-in/google', 'GoogleLoginController@redirectToProvider');
Route::get('sign-in/google/callback', 'GoogleLoginController@handleProviderCallback');


Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', 'PageController@index');        
    });
});

