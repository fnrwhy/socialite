<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function signUpPage() 
    {
        return view('page.sign-up');
    }

    public function signInPage()
    {
        return view('page.sign-in');
    }

    public function index()
    {
        return view('page.home');
    }
}
