<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\User;
use Auth;

class GoogleLoginController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $googleUser = Socialite::driver('google')->stateless()->user();

        $user = User::where('provider_id', $googleUser->getId())->first();

        if(!$user) {

            $user = User::create([
                'email'         => $googleUser->getEmail(),
                'name'          => $googleUser->getName(),
                'provider_id'   => $googleUser->getId(),
                'provider'      => 'google'
            ]);

        }

        // dd($facebookUser);

        Auth::login($user, true);

        return redirect('dashboard/')->with('ok', 'Selamat Datang.');
        // session(['token' => $user->token]);

    }
}
