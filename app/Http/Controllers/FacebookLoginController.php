<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\User;
use Auth;

class FacebookLoginController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $facebookUser = Socialite::driver('facebook')->user();

        $user = User::where('provider_id', $facebookUser->getId())->first();

        if(!$user) {

            $user = User::create([
                'email'         => $facebookUser->getEmail(),
                'name'          => $facebookUser->getName(),
                'provider_id'   => $facebookUser->getId(),
                'provider'      => 'facebook'
            ]);

        }

        // dd($facebookUser);

        Auth::login($user, true);

        return redirect('dashboard/')->with('ok', 'Selamat Datang.');
        // session(['token' => $user->token]);


    }

    
}
