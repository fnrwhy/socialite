<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
    public function signUp(Request $request) 
    {

        $userValidation = $request->validate ([
            'first_name' => 'required|max:255|min:6',
            'last_name'  => 'required|max:255|min:6',
            'email'      => 'required|email',
            'password'   => 'required|min:6'
        ]);

        $user = User::where('email', $request->email)->first();
        if($user != null) {
            return redirect()->back()->with('err', 'Email yang anda masukan sudah terdaftar');
        } else {            

            $user = User::create([
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name,
                'email'         => $request->email,
                'password'      => bcrypt($request->password),
                'provider'      => 'manual'
            ]);

            return redirect('sign-up')->with('ok', 'Berhasil mendaftarkan akun.');
        }


    }

    public function signIn(Request $request) 
    {
        $user = User::where('email', $request->email)->first();
        if($user != null) {
            $user = Auth::attempt(['email' => $request->email, 'password' => $request->password]);
                if($user) {
                    return redirect('dashboard/')->with('ok', 'Selamat Datang ' . Auth::user()->first_name . " " .  Auth::user()->last_name);
                } else {
                    return redirect()->back()->with('err', 'Email atau password yang anda masukkan salah.');
                }
        }
    }

    public function signOut() 
    {
        Auth::logout();
        return redirect('sign-in')->with('success', 'Anda telah logout');
    }

}
